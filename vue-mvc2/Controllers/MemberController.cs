﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using vue_mvc2.Models;

namespace vue_mvc2.Controllers
{
    public class MemberController : ApiController
    {
        public string GetAllMembers()
        {
            List<MyMember> oL = new List<MyMember>();
            oL.Add(new MyMember() { MemberName = "paladin", Gender = "男" });
            oL.Add(new MyMember() { MemberName = "ruby", Gender = "女" });

            return JsonConvert.SerializeObject(oL);
        }
    }
}
