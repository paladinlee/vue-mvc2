﻿var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!',
        members: []
    },
    created() {
        const vm = this;

        axios.get('../api/Member/GetAllMembers')
            .then(function (response) {
                // handle success
                console.log('api', response.data);
                vm.members = JSON.parse(response.data);
               
            })
            .catch(function (error) {
                // handle error
                console.log('api', error);
            })
            .then(function () {
                // always executed
            });
    }
})