﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vue_mvc2.Models
{
    public class MyMember
    {
        public string MemberName { get; set; }
        public string Gender { get; set; }
    }
}